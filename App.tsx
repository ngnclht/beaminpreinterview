/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react'
import { View } from 'react-native'
import { Provider } from 'react-redux'
import store from './src/configs/store'
import HomeScreen from './src/containers/home-screen/HomeScreen'

const App: () => React.ReactNode = () => {
  return (
    <Provider store={store}>
      <View>
        <HomeScreen />
      </View>
    </Provider>
  )
}

export default App
