module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        alias: {
          '@ui': './src/ui/',
          '@utils': './src/utils',
          '@sagas': './src/sagas',
          '@actions': './src/actions',
          '@action-types': './src/action-types',
          '@configs': './src/configs',
          '@reducers': './src/reducers',
          '@services': './src/services',
          '@constants': './src/constants',
          '@containers': './src/containers',
          '@hooks': './src/hooks',
          '@selectors': './src/selectors',
          '@api': './src/api',
          '@': './src'
        }
      }
    ]
  ]
}
