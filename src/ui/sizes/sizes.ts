import {
  widthPercentageToDP,
  heightPercentageToDP,
} from 'react-native-responsive-screen'

export const wp2dp: (number: number | string) => number = widthPercentageToDP
export const hp2dp: (number: number | string) => number = heightPercentageToDP

export const SIZES = {
  UI_PADDING_MARGIN: wp2dp('3.5%'),

  UI_FONT_SIZE_1: wp2dp('3%'), // 11
  UI_FONT_SIZE_1_LH: wp2dp('3.4%'), // 16

  UI_FONT_SIZE_2: wp2dp('3.2%'), // 13
  UI_FONT_SIZE_2_LH: wp2dp('3.5%'), // 18

  UI_FONT_SIZE_3: wp2dp('3.35%'), // 14
  UI_FONT_SIZE_3_LH: wp2dp('3.6%'), // 20

  UI_FONT_SIZE_4: wp2dp('4.2%'), // 16
  UI_FONT_SIZE_4LH: wp2dp('5.2%'), // 22

  UI_FONT_SIZE_5: wp2dp('4.5%'), // 18
  UI_FONT_SIZE_5LH: wp2dp('5.2%'), // 22

  HEADER_ICON: wp2dp('6.5%'), // 24
  PARTNER_BADGE_ICON: wp2dp('5.2%'), // 22

  MERCHANT_AVATER: wp2dp('24.2%'),

  MERCHANT_LIST_ITEM_HEIGHT: hp2dp(18),
}
