import React from 'react'
import { Text, StyleSheet, TextProps } from 'react-native'

const styles = StyleSheet.create({
  regular: {
    fontFamily: 'Roboto-Regular',
  },
  italic: {
    fontFamily: 'system font',
  },
  bold: {
    fontFamily: 'Roboto-Bold',
  },
})

const FONT_WEIGHTS: string[] = [
  '100',
  '200',
  '300',
  '400',
  '500',
  '600',
  '700',
  '800',
  '900',
  'normal',
  'bold',
]
const FONTS: any = {
  '100': 'Roboto-Regular',
  '200': 'Roboto-Regular',
  '300': 'Roboto-Regular',
  '400': 'Roboto-Regular',
  '500': 'Roboto-Bold',
  '600': 'Roboto-Bold',
  '700': 'Roboto-Bold',
  '800': 'Roboto-Bold',
  '900': 'Roboto-Bold',
  normal: 'Roboto-Regular',
  bold: 'Roboto-Bold',
}

export const FontText = (props: TextProps) => {
  const { style } = props
  let customStyle

  if (style) {
    if (style.fontWeight && FONT_WEIGHTS.includes(style.fontWeight)) {
      let fontFamily = `${FONTS[style.fontWeight]}`
      customStyle = { fontFamily }
    }
    if (style.fontStyle && style.fontStyle === 'italic') {
      customStyle = styles.italic
    }
  }

  return (
    <Text
      {...props}
      allowFontScaling={false}
      style={[styles.regular, style, customStyle]}
    >
      {props.children}
    </Text>
  )
}
