import { hp2dp, SIZES, wp2dp } from '@ui/sizes/sizes'
import THEMES from '@ui/themes/theme'
import { ViewStyle, TextStyle, ImageStyle, StyleSheet } from 'react-native'

type Style = {
  safeAreaView: ViewStyle
  container: ViewStyle
  leftContainer: ViewStyle
  rightContainer: ViewStyle
  buttonWrapper: ViewStyle
  menuButton: ViewStyle
  menuIcon: ImageStyle
  locationIcon: ImageStyle
  title: TextStyle
}

const styles = StyleSheet.create<Style>({
  safeAreaView: {
    flex: 1,
  },
  container: {
    marginTop: hp2dp(1),
    paddingHorizontal: SIZES.UI_PADDING_MARGIN,
    flexDirection: 'row',
    backgroundColor: THEMES.BACKGROUND_1,
    alignItems: 'center',
    width: wp2dp(100),
    height: hp2dp(5),
    justifyContent: 'space-between',
  },
  leftContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rightContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    fontSize: SIZES.UI_FONT_SIZE_5,
    lineHeight: SIZES.UI_FONT_SIZE_5LH,
    color: THEMES.TEXT_1,
    fontWeight: 'bold',
  },
  buttonWrapper: {
    paddingHorizontal: wp2dp(0.6),
    alignItems: 'center',
  },
  menuButton: {
    marginLeft: wp2dp(3),
  },
  locationIcon: {
    marginRight: wp2dp(3.3),
  },
  menuIcon: {
    height: SIZES.HEADER_ICON,
    width: SIZES.HEADER_ICON,
    resizeMode: 'contain',
  },
})

export default styles
