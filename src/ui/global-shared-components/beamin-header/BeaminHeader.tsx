import * as React from 'react'
import { View, TouchableOpacity, Image } from 'react-native'
import { FontText } from '../font-text'
import { BeaminHeaderAssets } from './assets'
import styles from './Styles'

type BeaminHeaderProps = {
  title: string
}

const BeaminHeader = (props: BeaminHeaderProps) => {
  return (
    <View style={styles.container}>
      <View style={styles.leftContainer}>
        <Image
          source={BeaminHeaderAssets.IconLocation}
          style={[styles.menuIcon, styles.locationIcon]}
        />

        <FontText style={styles.title}>{props.title}</FontText>
      </View>

      <View style={styles.rightContainer}>
        <TouchableOpacity style={styles.buttonWrapper}>
          <Image
            source={BeaminHeaderAssets.IconInbox}
            style={styles.notificationIcon}
          />
        </TouchableOpacity>
        <TouchableOpacity style={[styles.buttonWrapper, styles.menuButton]}>
          <Image source={BeaminHeaderAssets.IconMenu} style={styles.menuIcon} />
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default BeaminHeader
