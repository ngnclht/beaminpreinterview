import * as React from 'react'
import { View, Image } from 'react-native'
import { BeaminTextInput } from '../baemin-text-input/BeaminTextInput'
import { BeaminSearchBarAssets } from './assets'
import styles from './Styles'

type BeaminSearchBarProps = {
  onChangeText: (keyword: string) => void
}

const BeaminSearchBar = (props: BeaminSearchBarProps) => {
  return (
    <View style={styles.wrapper}>
      <View style={styles.container}>
        <Image
          source={BeaminSearchBarAssets.IconSearch}
          style={styles.searchIcon}
        />
        <BeaminTextInput
          onChangeText={props.onChangeText}
          placeholder="Search for a restaurant or menu"
          style={styles.textInput}
          placeholderStyle={styles.textInput}
        />
      </View>
    </View>
  )
}

export default BeaminSearchBar
