import { hp2dp, SIZES, wp2dp } from '@ui/sizes/sizes'
import THEMES from '@ui/themes/theme'
import { ViewStyle, ImageStyle, StyleSheet, TextInputStyle } from 'react-native'

type Style = {
  wrapper: ViewStyle
  container: ViewStyle
  textInput: TextInputStyle
  searchIcon: ImageStyle
}

const styles = StyleSheet.create<Style>({
  wrapper: {
    width: wp2dp(100),
    paddingHorizontal: SIZES.UI_PADDING_MARGIN,
    backgroundColor: THEMES.BACKGROUND_1,
    borderBottomColor: THEMES.DELIMITER,
    borderBottomWidth: 1,
    paddingBottom: hp2dp(2),
  },
  container: {
    marginTop: hp2dp(1),
    paddingVertical: hp2dp(2),
    paddingHorizontal: SIZES.UI_PADDING_MARGIN,
    flexDirection: 'row',
    backgroundColor: THEMES.BACKGROUND_2,
    alignItems: 'center',
    justifyContent: 'center',
    height: hp2dp(3),
    borderRadius: wp2dp(20),
  },
  searchIcon: {
    height: SIZES.HEADER_ICON,
    width: SIZES.HEADER_ICON,
    resizeMode: 'contain',
    marginRight: wp2dp(1),
  },
  textInput: {
    height: hp2dp(5),
    color: THEMES.TEXT_2,
    fontSize: SIZES.UI_FONT_SIZE_3,
    lineHeight: SIZES.UI_FONT_SIZE_3_LH,
  },
})

export default styles
