import * as React from 'react'
import { TextInput, TextInputProps, TextStyle } from 'react-native'

type BeaminTextInputProps = TextInputProps & {
  placeholderStyle: TextStyle
}

type BeaminTextInputState = { placeholder: boolean }

export class BeaminTextInput extends React.Component<
  BeaminTextInputProps,
  BeaminTextInputState
> {
  constructor(props) {
    super(props)
    this.state = { placeholder: props?.value?.length == 0 }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(ev) {
    this.setState({ placeholder: ev.nativeEvent.text.length == 0 })
    this.props.onChange && this.props.onChange(ev)
  }

  render() {
    const { placeholderStyle, style, onChange, ...rest } = this.props

    return (
      <TextInput
        {...rest}
        onChange={this.handleChange}
        style={this.state.placeholder ? [style, placeholderStyle] : style}
      />
    )
  }
}
