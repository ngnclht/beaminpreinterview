export default {
  MAIN: '#26BBC3',
  BACKGROUND_1: '#fff',
  BACKGROUND_2: '#F5F7FA',
  DELIMITER: '#EBF0F5',

  TEXT_1: '#141619',
  TEXT_2: '#ADB5C3',
  TEXT_3: '#2B343D',
  TEXT_4: '#646D7A',
  TEXT_5: '#7F8893',
}
