import { Appearance } from 'react-native'
import darkTheme from './dark-theme'
import lightTheme from './light-theme'

const colorScheme = Appearance.getColorScheme()

export const THEMES = colorScheme == 'light' ? lightTheme : darkTheme

export default THEMES
