import {
  actionFetchMerchantFailed,
  actionFetchMerchantRequest,
  actionFetchMerchantRequestType,
  actionFetchMerchantSuccess,
  FETCH_MERCHANT_REQUEST,
  RESET_MERCHANT_REQUEST_STATE,
} from '@actions/merchant'
import { put, takeLatest, call } from '@redux-saga/core/effects'
import { MAX_MERCHANT_PAGE } from '@services/MerchantService'
import { MerchantApiPayload } from 'models/merchant'
import MerchantService from '@services/MerchantService'
import { Action } from 'redux'

export const MerchantServices = new MerchantService()

export function* fetchMerchantWorker(action: actionFetchMerchantRequestType) {
  try {
    const { keyword, page } = action.payload
    if (page <= MAX_MERCHANT_PAGE) {
      const response: MerchantApiPayload = yield call(
        MerchantServices.getMerchantsAsyncWithFailedCase,
        page
      )
      yield put(
        actionFetchMerchantSuccess({
          merchants: response.data,
          hasMoreData: page < MAX_MERCHANT_PAGE,
        })
      )
    }
  } catch (error) {
    // TODO: send this error to Sentry or Bugsnag
    yield put(
      actionFetchMerchantFailed({
        error: error.message,
      })
    )
  }
}

export function* fetchMerchantWatcher() {
  yield takeLatest(FETCH_MERCHANT_REQUEST, fetchMerchantWorker)
}

export function* refreshMerchantListWorker(action: Action) {
  yield put(
    actionFetchMerchantRequest({
      page: 1,
    })
  )
}

export function* refreshMerchantListWatcher() {
  yield takeLatest(RESET_MERCHANT_REQUEST_STATE, refreshMerchantListWorker)
}
