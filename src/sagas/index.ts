import { all, call } from 'redux-saga/effects'
import { fetchMerchantWatcher, refreshMerchantListWatcher } from './merchant'

export default function* rootSaga() {
  yield all([call(fetchMerchantWatcher), call(refreshMerchantListWatcher)])
}
