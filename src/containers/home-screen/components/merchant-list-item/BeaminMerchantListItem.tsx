import { FontText } from '@ui/global-shared-components/font-text'
import { getDistanceString, getNumOfOrder } from '@utils/merchant'
import Merchant from 'models/merchant'
import * as React from 'react'
import { View, TouchableOpacity, Image } from 'react-native'
import {
  Placeholder,
  PlaceholderMedia,
  PlaceholderLine,
  Fade,
} from 'rn-placeholder'

import { MerchantListItemAssets } from './assets'
import styles from './Styles'

type BeaminMerchantListItemProps = {
  merchant?: Merchant
  showLoading?: boolean
}

const BeaminMerchantListItem = (props: BeaminMerchantListItemProps) => {
  if (props.showLoading) {
    return (
      <View style={styles.container}>
        <Placeholder
          Animation={Fade}
          Left={() => (
            <View
              style={[styles.imageWrapper, styles.imageWrapperWithMarginRight]}
            >
              <PlaceholderMedia />
            </View>
          )}
        >
          <PlaceholderLine />
          <PlaceholderLine />
          <PlaceholderLine width={30} />
        </Placeholder>
      </View>
    )
  }

  if (!props.merchant) return null

  const merchantName =
    props.merchant?.name || `Merchant number ${props.merchant?.id}`
  const numOfOrder = getNumOfOrder(props.merchant.numOfOrder)
  return (
    <TouchableOpacity style={styles.container}>
      <View style={styles.imageWrapper}>
        <Image
          source={{ uri: props.merchant.image }}
          style={styles.merchantImage}
        />
      </View>
      <View style={styles.textWrapper}>
        <View style={styles.merchantNameAndBadgeWrapper}>
          <Image
            source={MerchantListItemAssets.IconPartnerBadge}
            style={styles.partnerBadgeIcon}
          />
          <FontText
            numberOfLines={2}
            ellipsizeMode="tail"
            style={styles.merchantNameText}
          >
            {`${`      `}${merchantName}`}
          </FontText>
        </View>
        <FontText
          numberOfLines={1}
          ellipsizeMode="tail"
          style={styles.merchantDescriptionText}
        >
          {props.merchant.description}
        </FontText>
        <View style={styles.rattingAndDistanceWrapper}>
          <Image
            source={MerchantListItemAssets.IconStar}
            style={styles.starIcon}
          />
          <FontText style={styles.merchantRatingText}>
            {props.merchant.rating || '--'}{' '}
          </FontText>

          {numOfOrder ? (
            <FontText style={styles.merchantNumOfferText}>
              ({numOfOrder})
            </FontText>
          ) : null}
          {props.merchant.distance ? (
            <>
              <FontText style={styles.bullet}>.</FontText>
              <FontText style={styles.merchantDistanceText}>
                {getDistanceString(props.merchant.distance)}
              </FontText>
            </>
          ) : null}
        </View>
      </View>
    </TouchableOpacity>
  )
}

export default BeaminMerchantListItem
