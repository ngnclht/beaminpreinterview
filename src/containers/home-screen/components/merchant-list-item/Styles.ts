import { hp2dp, SIZES, wp2dp } from '@ui/sizes/sizes'
import THEMES from '@ui/themes/theme'
import { ImageStyle, TextStyle, ViewStyle, StyleSheet } from 'react-native'

type Style = {
  container: ViewStyle
  imageWrapper: ViewStyle
  imageWrapperWithMarginRight: ViewStyle
  textWrapper: ViewStyle
  merchantNameAndBadgeWrapper: ViewStyle
  rattingAndDistanceWrapper: ViewStyle
  merchantImage: ImageStyle
  starIcon: ImageStyle
  partnerBadgeIcon: ImageStyle
  merchantNameText: TextStyle
  merchantDescriptionText: TextStyle
  merchantRatingText: TextStyle
  merchantNumOfferText: TextStyle
  bullet: TextStyle
  merchantDistanceText: TextStyle
}

const styles = StyleSheet.create<Style>({
  container: {
    backgroundColor: THEMES.BACKGROUND_1,
    flexDirection: 'row',
    paddingHorizontal: SIZES.UI_PADDING_MARGIN,
    borderBottomWidth: 1,
    borderBottomColor: THEMES.DELIMITER,
    width: wp2dp(100),
    height: SIZES.MERCHANT_LIST_ITEM_HEIGHT,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageWrapper: {
    borderRadius: 12,
    borderWidth: 1,
    borderColor: THEMES.DELIMITER,
    height: SIZES.MERCHANT_AVATER,
    width: SIZES.MERCHANT_AVATER,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageWrapperWithMarginRight: {
    marginRight: wp2dp(2),
  },
  textWrapper: {
    flex: 1,
    maxHeight: SIZES.MERCHANT_AVATER * 0.95,
    marginLeft: SIZES.UI_PADDING_MARGIN,
  },
  merchantNameAndBadgeWrapper: {
    flexDirection: 'row',
  },
  rattingAndDistanceWrapper: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  merchantImage: {
    height: SIZES.MERCHANT_AVATER,
    width: SIZES.MERCHANT_AVATER,
    resizeMode: 'contain',
    borderRadius: 12,
  },
  starIcon: {
    height: wp2dp(4.5),
    width: wp2dp(4.5),
    resizeMode: 'contain',
  },
  partnerBadgeIcon: {
    height: wp2dp(4.7),
    width: wp2dp(4.7),
    resizeMode: 'contain',
    position: 'absolute',
  },
  merchantNameText: {
    color: THEMES.TEXT_1,
    fontSize: SIZES.UI_FONT_SIZE_5,
    lineHeight: SIZES.UI_FONT_SIZE_5LH,
    flex: 1,
  },
  merchantDescriptionText: {
    flex: 1,
    color: THEMES.TEXT_4,
    fontSize: SIZES.UI_FONT_SIZE_2,
    lineHeight: SIZES.UI_FONT_SIZE_2_LH,
    marginTop: 5,
  },
  merchantRatingText: {
    color: THEMES.TEXT_3,
    fontSize: SIZES.UI_FONT_SIZE_2,
    lineHeight: SIZES.UI_FONT_SIZE_2_LH,
  },
  merchantNumOfferText: {
    color: THEMES.TEXT_5,
    fontSize: SIZES.UI_FONT_SIZE_1,
    lineHeight: SIZES.UI_FONT_SIZE_1_LH,
  },
  bullet: {
    marginTop: -15,
    fontSize: SIZES.UI_FONT_SIZE_5 * 1.5,
    marginHorizontal: SIZES.UI_PADDING_MARGIN,
  },
  merchantDistanceText: {
    color: THEMES.TEXT_3,
    fontSize: SIZES.UI_FONT_SIZE_2,
    lineHeight: SIZES.UI_FONT_SIZE_2_LH,
  },
})

export default styles
