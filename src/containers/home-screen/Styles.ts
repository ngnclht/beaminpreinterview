import { hp2dp, SIZES, wp2dp } from '@ui/sizes/sizes'
import THEMES from '@ui/themes/theme'
import { TextStyle, ViewStyle, StyleSheet } from 'react-native'

type Style = {
  safeAreaView: ViewStyle
  container: ViewStyle
  errorView: ViewStyle
  emptyView: ViewStyle
  errorViewRetryButton: ViewStyle
  errorViewRetryButtonTitle: TextStyle
  errorViewLabelText: TextStyle
  title: TextStyle
}

const styles = StyleSheet.create<Style>({
  safeAreaView: {
    flex: 1,
  },
  container: {
    backgroundColor: THEMES.BACKGROUND_1,
    paddingBottom: hp2dp(5),
    height: hp2dp(100),
  },
  errorView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  errorViewRetryButton: {
    alignItems: 'center',
    paddingHorizontal: wp2dp(5),
    paddingVertical: 5,
    borderRadius: 5,
    backgroundColor: THEMES.BACKGROUND_2,
  },
  errorViewRetryButtonTitle: {
    color: THEMES.TEXT_1,
    fontSize: SIZES.UI_FONT_SIZE_2,
    lineHeight: SIZES.UI_FONT_SIZE_2_LH,
  },
  errorViewLabelText: {
    marginBottom: 10,
    color: THEMES.TEXT_1,
    fontSize: SIZES.UI_FONT_SIZE_2,
    lineHeight: SIZES.UI_FONT_SIZE_2_LH,
  },
  title: {
    color: THEMES.TEXT_3,
    fontSize: SIZES.UI_FONT_SIZE_5,
    lineHeight: SIZES.UI_FONT_SIZE_5LH,
    fontWeight: 'bold',
    paddingHorizontal: SIZES.UI_PADDING_MARGIN,
    marginTop: SIZES.UI_PADDING_MARGIN,
  },
  emptyView: {
    paddingTop: 2,
    height: hp2dp(3),
    width: wp2dp(100),
  },
})

export default styles
