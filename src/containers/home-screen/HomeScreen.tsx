import BeaminHeader from '@ui/global-shared-components/beamin-header/BeaminHeader'
import BeaminSearchBar from '@ui/global-shared-components/beamin-search-bar/BeaminSearchBar'
import * as React from 'react'
import _ from 'lodash'
import {
  View,
  ActivityIndicator,
  SafeAreaView,
  FlatList,
  InteractionManager,
  TouchableOpacity,
  RefreshControl,
} from 'react-native'
import { FontText } from '@ui/global-shared-components/font-text'
import { COUNT_MERCHANT_PER_PAGE } from '@services/MerchantService'
import BeaminMerchantListItem from './components/merchant-list-item/BeaminMerchantListItem'
import styles from './Styles'
import { SIZES } from '@ui/sizes/sizes'
import { useDispatch, useSelector } from 'react-redux'
import {
  actionFetchMerchantRequest,
  actionRefreshMerchantFetchState,
} from '@actions/merchant'
import {
  selectCurrentPage,
  selectErrorMessage,
  selectHasMoreData,
  selectIsFetching,
  selectMerchants,
} from '@selectors/merchant'
import THEMES from '@ui/themes/theme'

const HomeScreen = () => {
  const dispatch = useDispatch()
  const currentPage = useSelector(selectCurrentPage)
  const isFetching = useSelector(selectIsFetching)
  const errorMessage = useSelector(selectErrorMessage)
  const merchants = useSelector(selectMerchants)
  const hasMoreData = useSelector(selectHasMoreData)

  const onChangeText = React.useCallback(
    _.throttle((text: string) => {
      console.log('Text ' + text)
    }, 2000),
    []
  )

  const onRefresh = React.useCallback(() => {
    dispatch(actionRefreshMerchantFetchState())
  }, [])

  const showLoadingFullPage = isFetching && currentPage == 0
  const showLoadMoreLoading = isFetching && currentPage > 0

  const showErrorMsgFullPage = !isFetching && !!errorMessage && currentPage == 0
  const showErrorMsgLoading = !isFetching && !!errorMessage && currentPage > 0

  const onLoadMore = React.useCallback(
    _.throttle(() => {
      // dont do anything if no more data left
      if (!hasMoreData) return
      dispatch(actionFetchMerchantRequest({ page: currentPage + 1 }))
    }, 500),
    [currentPage]
  )

  React.useEffect(() => {
    InteractionManager.runAfterInteractions(() => {
      dispatch(actionFetchMerchantRequest({ page: 1 }))
    })
  }, [])

  const renderItem = ({ item }) => <BeaminMerchantListItem merchant={item} />

  const title = <FontText style={styles.title}>Restaurants near you</FontText>

  return (
    <SafeAreaView style={styles.safeAreaView}>
      <View style={styles.container}>
        <BeaminHeader title="Some where" />
        <BeaminSearchBar onChangeText={onChangeText} />
        {showErrorMsgFullPage ? (
          <View style={styles.errorView}>
            <FontText style={styles.errorViewLabelText}>
              An error has occured!
            </FontText>
            <TouchableOpacity
              onPress={onRefresh}
              style={styles.errorViewRetryButton}
            >
              <FontText style={styles.errorViewRetryButtonTitle}>
                Retry again!
              </FontText>
            </TouchableOpacity>
          </View>
        ) : showLoadingFullPage ? (
          <FlatList
            ListHeaderComponent={() => title}
            data={_.range(5)}
            renderItem={() => <BeaminMerchantListItem showLoading />}
            keyExtractor={(item) => `${item}`}
          />
        ) : (
          <FlatList
            ListHeaderComponent={() => title}
            ListFooterComponent={
              <View style={styles.emptyView}>
                {showLoadMoreLoading ? (
                  <ActivityIndicator color={THEMES.MAIN} />
                ) : showErrorMsgLoading ? (
                  <TouchableOpacity
                    onPress={onLoadMore}
                    style={styles.errorViewRetryButton}
                  >
                    <FontText style={styles.errorViewRetryButtonTitle}>
                      There is an error. Click to load more again!
                    </FontText>
                  </TouchableOpacity>
                ) : null}
              </View>
            }
            getItemLayout={(_data, index) => ({
              length: SIZES.MERCHANT_LIST_ITEM_HEIGHT,
              offset: SIZES.MERCHANT_LIST_ITEM_HEIGHT * index,
              index,
            })}
            data={merchants}
            renderItem={renderItem}
            keyExtractor={(item, index) => `${item.id}${index}`}
            refreshControl={
              <RefreshControl
                refreshing={showErrorMsgFullPage}
                onRefresh={onRefresh}
              />
            }
            onEndReachedThreshold={0.1}
            onEndReached={({ distanceFromEnd }) => {
              onLoadMore()
            }}
          />
        )}
      </View>
    </SafeAreaView>
  )
}

export default HomeScreen
