import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootReducer from '@reducers/index'
import rootSaga from '@sagas/index'
import ReactoronConfig from '@configs/reactoron'
import { IS_DEV, IS_TEST } from '@configs/index'

export const setupStore = () => {
  const sagaMiddleware = createSagaMiddleware()
  const middleWares = applyMiddleware(sagaMiddleware)
  const enhancer =
    IS_DEV && !IS_TEST
      ? compose(middleWares, ReactoronConfig.createEnhancer())
      : compose(middleWares)

  const store = createStore(rootReducer, enhancer)

  sagaMiddleware.run(rootSaga)
  return store
}

const store = setupStore()
export default store
