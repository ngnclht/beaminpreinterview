import { NativeModules } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import Reactotron from 'reactotron-react-native'
import { reactotronRedux } from 'reactotron-redux'
import { IS_DEV, IS_TEST } from '@configs/index'

let scriptHostname
let ReactoronConfig: any = {}
if (IS_DEV) {
  const scriptURL = NativeModules.SourceCode.scriptURL
  scriptHostname = scriptURL.split('://')[1].split(':')[0]
  // @ts-ignore
  if (!IS_TEST) console.log = Reactotron.log
  // @ts-ignore
  ReactoronConfig = Reactotron.setAsyncStorageHandler(AsyncStorage)
    .configure({ host: scriptHostname })
    .use(reactotronRedux())
    .useReactNative()
    .connect()
}

export default ReactoronConfig
