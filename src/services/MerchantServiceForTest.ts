import MerchantService from '@services/MerchantService'
import Merchant from 'models/merchant'
const merchantService = new MerchantService()

export const merchantsForTest: Merchant[] =
  merchantService.getMerchantsSync(1).data

export const merchantsForTest2: Merchant[] =
  merchantService.getMerchantsSync(2).data
