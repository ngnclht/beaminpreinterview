export const getNumOfOrder = (num: number): string => {
  if (!num) return ''
  if (num > 999) return '999+'
  if (num > 500) return '500+'
  if (num > 100) return '100+'
  if (num > 50) return '50+'
  if (num > 10) return '10+'
  return `${num}`
}

export const getDistanceString = (distance: number): string => {
  return `${distance}km`
}
