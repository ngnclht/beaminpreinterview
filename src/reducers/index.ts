import { combineReducers } from 'redux'
import merchant, { MerchantReducerType } from './merchant'

export type RootReducerType = {
  merchant: MerchantReducerType
}

const rootReducer = combineReducers({
  merchant,
})

export default rootReducer
