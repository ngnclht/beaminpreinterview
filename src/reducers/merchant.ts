import {
  actionFetchMerchantFailedType,
  actionFetchMerchantRequestType,
  actionFetchMerchantSuccessType,
  RESET_MERCHANT_REQUEST_STATE,
  FETCH_MERCHANT_FAILED,
  FETCH_MERCHANT_REQUEST,
  FETCH_MERCHANT_SUCCESS,
} from '@actions/merchant'
import Merchant from 'models/merchant'
import { ActionType } from '../actions/index'

export type MerchantReducerType = {
  merchants: Merchant[]
  isFetching: boolean
  fetchErrorMessage: string
  currentPage: number
  currentKeyword?: string
  fetchParams?: {
    page: number
    keyword?: string
  }
  hasMoreData: boolean
}

export const initialStateOfMerchantReducer: MerchantReducerType = {
  merchants: [],
  isFetching: false,
  fetchErrorMessage: '',
  currentPage: 0,
  hasMoreData: true,
}

export default function merchant(
  state = initialStateOfMerchantReducer,
  action: ActionType
) {
  switch (action.type) {
    case RESET_MERCHANT_REQUEST_STATE:
      return {
        ...state,
        currentPage: 0,
        currentKeyword: '',
        hasMoreData: true,
        merchants: [],
        fetchParams: {
          page: 0,
          keyword: '',
        },
      }
    case FETCH_MERCHANT_REQUEST:
      const actionFetchMerchantRequest =
        action as actionFetchMerchantRequestType
      return {
        ...state,
        isFetching: true,
        fetchErrorMessage: '',
        fetchParams: {
          page: actionFetchMerchantRequest.payload.page,
          keyword: actionFetchMerchantRequest.payload.keyword,
        },
      }

    case FETCH_MERCHANT_SUCCESS:
      const actionFetchMerchantSuccess =
        action as actionFetchMerchantSuccessType
      return {
        ...state,
        isFetching: false,
        currentPage: state.fetchParams.page,
        currentKeyword: state.fetchParams.keyword,
        fetchErrorMessage: '',
        merchants:
          state.fetchParams.page == 1
            ? actionFetchMerchantSuccess.payload.merchants
            : [
                ...state.merchants,
                ...actionFetchMerchantSuccess.payload.merchants,
              ],
        hasMoreData: actionFetchMerchantSuccess.payload.hasMoreData,
      }

    case FETCH_MERCHANT_FAILED:
      const actionFetchMerchantFailed = action as actionFetchMerchantFailedType
      return {
        ...state,
        fetchErrorMessage: actionFetchMerchantFailed.payload.error,
        isFetching: false,
      }
    default:
      return state
  }
}
