import { RootReducerType } from '@reducers/index'

export const selectFetchParams = (state: RootReducerType) =>
  state.merchant.fetchParams
export const selectCurrentPage = (state: RootReducerType) =>
  state.merchant.currentPage
export const selectCurrentKeyword = (state: RootReducerType) =>
  state.merchant.currentKeyword
export const selectErrorMessage = (state: RootReducerType) =>
  state.merchant.fetchErrorMessage
export const selectIsFetching = (state: RootReducerType) =>
  state.merchant.isFetching
export const selectHasMoreData = (state: RootReducerType) =>
  state.merchant.hasMoreData
export const selectMerchants = (state: RootReducerType) =>
  state.merchant.merchants
