import Merchant from 'models/merchant'
import { ActionType } from '.'

export const RESET_MERCHANT_REQUEST_STATE =
  'merchant/reset_fetch_merchant_state'
export const FETCH_MERCHANT_REQUEST = 'merchant/fetch_merchant_request'
export const FETCH_MERCHANT_SUCCESS = 'merchant/fetch_merchant_success'
export const FETCH_MERCHANT_FAILED = 'merchant/fetch_merchant_failed'

export type actionFetchMerchantRequestType = ActionType & {
  payload: {
    page: number
    keyword?: string
  }
}

export const actionRefreshMerchantFetchState: () => ActionType = () => ({
  type: RESET_MERCHANT_REQUEST_STATE,
})

export const actionFetchMerchantRequest: (p: {
  page: number
  keyword?: string
}) => actionFetchMerchantRequestType = ({ page, keyword }) => ({
  type: FETCH_MERCHANT_REQUEST,
  payload: {
    page,
    keyword,
  },
})

export type actionFetchMerchantSuccessType = ActionType & {
  payload: {
    merchants: Merchant[]
    hasMoreData?: boolean
  }
}

export const actionFetchMerchantSuccess: (p: {
  merchants: Merchant[]
  hasMoreData?: boolean
}) => actionFetchMerchantSuccessType = ({ merchants, hasMoreData }) => ({
  type: FETCH_MERCHANT_SUCCESS,
  payload: {
    merchants,
    hasMoreData,
  },
})

export type actionFetchMerchantFailedType = ActionType & {
  payload: {
    error: string
  }
}

export const actionFetchMerchantFailed: ({
  error: string,
}) => actionFetchMerchantFailedType = ({ error }) => ({
  type: FETCH_MERCHANT_FAILED,
  payload: {
    error,
  },
})
