const real = jest.requireActual('redux-persist')
const index = {
  ...real,
}

export const persistReducer = jest
  .fn()
  .mockImplementation((config, reducers) => reducers)
export const createTransform = () => {}
export const persistStore = () => {}

export default index
