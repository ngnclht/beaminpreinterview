import {
  actionFetchMerchantFailed,
  actionFetchMerchantRequest,
  actionFetchMerchantSuccess,
} from '@actions/merchant'
import { setupStore } from '@configs/store'
import { initialStateOfMerchantReducer } from '@reducers/merchant'
import {
  selectCurrentKeyword,
  selectCurrentPage,
  selectErrorMessage,
  selectFetchParams,
  selectIsFetching,
  selectMerchants,
} from '@selectors/merchant'
import {
  merchantsForTest,
  merchantsForTest2,
} from '@services/MerchantServiceForTest'

let store

const getState = () => {
  let appState = store.getState()
  return appState
}

beforeEach(() => {
  store = setupStore()
})

test('Initial state check', () => {
  let state = getState()
  expect(state.merchant).toStrictEqual(initialStateOfMerchantReducer)
})

test('State after fetch merchant request action has been dispatched', () => {
  const params = {
    page: 2,
    keyword: 'phuc long',
  }
  store.dispatch(actionFetchMerchantRequest(params))
  let state = getState()
  expect(selectCurrentPage(state)).toEqual(0)
  expect(selectFetchParams(state)).toStrictEqual(params)
  expect(selectIsFetching(state)).toEqual(true)
  expect(selectErrorMessage(state)).toEqual('')
})

test('State after fetch merchant success action has been dispatched', () => {
  const params = {
    page: 1,
    keyword: 'phuc long',
  }
  store.dispatch(actionFetchMerchantRequest(params))
  store.dispatch(
    actionFetchMerchantSuccess({
      merchants: merchantsForTest,
      hasMoreData: true,
    })
  )
  let state = getState()
  expect(selectIsFetching(state)).toEqual(false)
  expect(selectCurrentPage(state)).toEqual(params.page)
  expect(selectCurrentKeyword(state)).toEqual(params.keyword)
  expect(selectErrorMessage(state)).toEqual('')
  expect(selectMerchants(state)).toStrictEqual(merchantsForTest)
})

test('It should merge data after fetch merchant success in the seconds time when page > 1', () => {
  let params = {
    page: 1,
    keyword: 'phuc long',
  }
  store.dispatch(actionFetchMerchantRequest(params))
  store.dispatch(
    actionFetchMerchantSuccess({
      merchants: merchantsForTest,
      hasMoreData: true,
    })
  )
  let state = getState()
  expect(selectMerchants(state)).toStrictEqual(merchantsForTest)

  params = {
    page: 2,
    keyword: 'phuc long',
  }
  store.dispatch(actionFetchMerchantRequest(params))
  store.dispatch(
    actionFetchMerchantSuccess({
      merchants: merchantsForTest2,
      hasMoreData: true,
    })
  )
  state = getState()
  expect(selectMerchants(state)).toStrictEqual([
    ...merchantsForTest,
    ...merchantsForTest2,
  ])
})

test('State after fetch merchant failed action has been dispatched', () => {
  const params = {
    page: 2,
    keyword: 'phuc long',
  }
  store.dispatch(actionFetchMerchantRequest(params))
  const error = 'An error has occured. We are sorry for this in convinience'
  store.dispatch(
    actionFetchMerchantFailed({
      error,
    })
  )
  let state = getState()
  expect(selectIsFetching(state)).toEqual(false)
  expect(selectCurrentPage(state)).toBeLessThan(params.page)
  expect(selectCurrentKeyword(state)).toEqual(undefined)
  expect(selectErrorMessage(state)).toEqual(error)
})
