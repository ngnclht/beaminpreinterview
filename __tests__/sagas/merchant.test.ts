import {
  actionFetchMerchantFailed,
  actionFetchMerchantRequest,
  actionFetchMerchantSuccess,
  actionRefreshMerchantFetchState,
  FETCH_MERCHANT_REQUEST,
  RESET_MERCHANT_REQUEST_STATE,
} from '@actions/merchant'
import {
  fetchMerchantWatcher,
  fetchMerchantWorker,
  MerchantServices,
  refreshMerchantListWatcher,
  refreshMerchantListWorker,
} from '@sagas/merchant'
import { takeLatest } from '@redux-saga/core/effects'
import { runSaga } from 'redux-saga'
import { MAX_MERCHANT_PAGE } from '@services/MerchantService'
import { put } from 'redux-saga/effects'

describe('fetchMerchantFromAPI', () => {
  const genObject = fetchMerchantWatcher()

  it('should wait for every FETCH_MERCHANT_REQUEST action and call fetchMerchantWorker', () => {
    expect(genObject.next().value).toEqual(
      takeLatest(FETCH_MERCHANT_REQUEST, fetchMerchantWorker)
    )
  })

  it('should be done on next iteration', () => {
    expect(genObject.next().done).toBeTruthy()
  })

  it('should not dispatch any action on next iteration max page reached', async () => {
    let lastMerchantResponse
    // to make sure API always success
    const getMerchantSuccess = jest
      .spyOn(MerchantServices, 'getMerchantsAsyncWithFailedCase')
      .mockImplementation((page: number) => {
        return new Promise((resolve) => {
          setTimeout(() => {
            lastMerchantResponse = MerchantServices.getMerchantsSync(page)
            resolve(lastMerchantResponse)
          }, 1000)
        })
      })

    const dispatched = []
    await runSaga(
      {
        dispatch: (action) => dispatched.push(action),
      },
      fetchMerchantWorker,
      actionFetchMerchantRequest({ page: MAX_MERCHANT_PAGE + 1 })
    ).toPromise()

    expect(getMerchantSuccess).toHaveBeenCalledTimes(0)
    expect(dispatched[0]).toBeUndefined()
  })

  it('should dispatch a FETCH_MERCHANT_FAILED on next iteration if there is an error', async () => {
    const error = 'Sorry, something wrong'
    const getMerchant = jest
      .spyOn(MerchantServices, 'getMerchantsAsyncWithFailedCase')
      .mockImplementation((page: number) => {
        return Promise.reject(new Error(error))
      })

    const dispatched = []
    await runSaga(
      {
        dispatch: (action) => dispatched.push(action),
      },
      fetchMerchantWorker,
      actionFetchMerchantRequest({ page: 1 })
    ).toPromise()

    expect(getMerchant).toHaveBeenCalledTimes(1)
    expect(dispatched[0]).toStrictEqual(actionFetchMerchantFailed({ error }))
  })

  it('should dispatch a FETCH_MERCHANT_SUCCESS on next iteration if things are good', async () => {
    let lastMerchantResponse
    // to make sure API always success
    const getMerchantSuccess = jest
      .spyOn(MerchantServices, 'getMerchantsAsyncWithFailedCase')
      .mockImplementation((page: number) => {
        return new Promise((resolve) => {
          setTimeout(() => {
            lastMerchantResponse = MerchantServices.getMerchantsSync(page)
            resolve(lastMerchantResponse)
          }, 1000)
        })
      })

    const dispatched = []
    await runSaga(
      {
        dispatch: (action) => dispatched.push(action),
      },
      fetchMerchantWorker,
      actionFetchMerchantRequest({ page: 1 })
    ).toPromise()

    expect(dispatched[0]).toStrictEqual(
      actionFetchMerchantSuccess({
        hasMoreData: true,
        merchants: lastMerchantResponse.data,
      })
    )
  })
})

describe('refreshMerchantListWatcher', () => {
  const genObject = refreshMerchantListWatcher()

  it('should wait for every RESET_MERCHANT_REQUEST_STATE action and call refreshMerchantListWorker', () => {
    expect(genObject.next().value).toEqual(
      takeLatest(RESET_MERCHANT_REQUEST_STATE, refreshMerchantListWorker)
    )
  })
  it('it should call actionFetchMerchantRequest with page = 1', async () => {
    const dispatched = []
    await runSaga(
      {
        dispatch: (action) => dispatched.push(action),
      },
      refreshMerchantListWorker,
      actionRefreshMerchantFetchState()
    ).toPromise()

    expect(dispatched[0]).toStrictEqual(
      actionFetchMerchantRequest({
        page: 1,
      })
    )
  })

  it('should be done on next iteration', () => {
    expect(genObject.next().done).toBeTruthy()
  })
})
