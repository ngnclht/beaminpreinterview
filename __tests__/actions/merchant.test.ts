import { ActionType } from '@actions/'
import { merchantsForTest } from '@services/MerchantServiceForTest'
import {
  actionFetchMerchantFailed,
  actionFetchMerchantFailedType,
  actionFetchMerchantRequest,
  actionFetchMerchantSuccessType,
  actionFetchMerchantRequestType,
  actionFetchMerchantSuccess,
  actionRefreshMerchantFetchState,
  RESET_MERCHANT_REQUEST_STATE,
  FETCH_MERCHANT_FAILED,
  FETCH_MERCHANT_REQUEST,
  FETCH_MERCHANT_SUCCESS,
} from '../../src/actions/merchant'

test('action FETCH_MERCHANT_FAILED action good to go', () => {
  const action: ActionType = actionRefreshMerchantFetchState()
  expect(action.type).toEqual(RESET_MERCHANT_REQUEST_STATE)
})

test('action FETCH_MERCHANT_FAILED action good to go', () => {
  const error = 'Sorry, an error has occured'
  const action: actionFetchMerchantFailedType = actionFetchMerchantFailed({
    error,
  })
  expect(action.type).toEqual(FETCH_MERCHANT_FAILED)
  expect(action.payload.error).toEqual(error)
})

test('action FETCH_MERCHANT_SUCCESS action good to go', () => {
  const merchant = merchantsForTest[0]
  const action: actionFetchMerchantSuccessType = actionFetchMerchantSuccess({
    merchants: [merchant],
    hasMoreData: true,
  })
  expect(action.type).toEqual(FETCH_MERCHANT_SUCCESS)
  expect(action.payload.merchants).toEqual([merchant])
  expect(action.payload.hasMoreData).toEqual(true)
})

test('action FETCH_MERCHANT_REQUEST action good to go', () => {
  const keyword = 'phuc long'
  const action: actionFetchMerchantRequestType = actionFetchMerchantRequest({
    page: 1,
    keyword,
  })
  expect(action.type).toEqual(FETCH_MERCHANT_REQUEST)
  expect(action.payload.keyword).toEqual(keyword)
  expect(action.payload.page).toEqual(1)
})
